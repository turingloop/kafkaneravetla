
If you are reading this file, you were able to clone this repository. To build and run the consumer and producer,issue the follwoing command.

1. "mvn compile assembly:single" - this command builds the software(maven has to be installed before you issue the command)

2. please install zookeeper and kafka. Run zookeeper in terminal window and then start running kafka in another window. Both of them need to be running.

3. now issue - "java -cp target/KafkaAPIClient-1.0-SNAPSHOT-jar-with-dependencies.jar com.neravetla.fka.random.Consumer test group1" command within the repository folder.

4. now issue in a new terminal window - "java -cp target/KafkaAPIClient-1.0-SNAPSHOT-jar-with-dependencies.jar com.neravetla.kafka.random.CustomProducer test" - this should run the producer. 

5. Now you should be able to see the output on the consumer window.

Note: CustomerProducer.java in the main folder is only to make it easier to look at the customproducer code. If you want to run it, issue the command in instruction number 3.
