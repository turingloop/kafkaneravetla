package com.neravetla.kafka.random;

//import com.neravetla.kafka.simple.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by sunilpatil on 12/28/15.
 */
public class CustomProducer {
    private static Scanner in;
    public static void main(String[] argv)throws Exception {
        if (argv.length != 1) {
            System.err.println("Please specify 1 parameters ");
            System.exit(-1);
        }
        String[] shakespearePlays = {"As you like it", "Romeo and Juliet", "Hamlet", "Othello", "The Tempest", "Julius Caesar", "King Lear", "Macbeth"};
        String topicName = argv[0];
        Random rand = new Random();
        in = new Scanner(System.in);
        System.out.println("This producer will publish a random Shakespeare play name to the Kafka every 4 seconds!");
        //Configure the CustomProducer
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArraySerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");

        org.apache.kafka.clients.producer.Producer producer = new KafkaProducer(configProperties);
        //String line = in.nextLine();
        while(true) {
            //TODO: Make sure to use the ProducerRecord constructor that does not take parition Id
            
            int randIndex = rand.nextInt((8));
            String playName = shakespearePlays[randIndex];
            ProducerRecord<String, String> rec = new ProducerRecord<String, String>(topicName,playName);
            producer.send(rec);
            System.out.println("Just sent to Kafka: "+playName);
            System.out.println("Waiting for 5 seconds");
            TimeUnit.SECONDS.sleep(5);
            //line = in.nextLine();
        }
        //in.close();
        //producer.close();
    }
}
